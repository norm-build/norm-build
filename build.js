#!/usr/bin/env node
process.chdir(__dirname);
const fs = require('fs-extra');
const path = require('path');
const child_process = require('child_process');

function clean() {
  return Promise.all([
    fs.remove('dist'),
    fs.remove('lib')
  ])
}

function babel() {
  return fs.ensureDir('lib')
    .then(() => {
      return new Promise((pass, fail) => {  
        child_process.exec("yarn babel src --out-dir lib --copy-files", (error, stdout, stderr) => {
          if(error || stderr) {
            fail(error || stderr);
          } else {
            pass(stdout);
          }
        });
      });
    })
}

const args = process.argv.slice(2);
if(!args.filter(a => a[0] !== '-').length) {
  args.push(process.cwd());
}

const packageJson = require('./package.json');

function build() {
  // $ExpectError: ./lib doesn't exist until after the build
  return require('./lib/indexa').default((m) => console.log(m), {
    production: false,
    module: true,
    executable: true,
    packageJson: {
      name: 'norm-build',
      main: './index.js',
      version: packageJson.version,
      license: packageJson.license,
      author: packageJson.author,
      repository: packageJson.repository,
      private: false,
      bin: {
        build: './index.js'
      },
      dependencies: packageJson.dependencies,
    }
  }, ...args)
}

clean()
  .then(babel)
  .then(build)
  .then(m => console.log(m))
  .catch(error => {
    console.log(`An error occurred`);
    const message = error.toString();
    if(/^\s+$/.test(message)) {
      console.error(`No error message was provided`);
    } else {
      console.error(message);
    }
    const stack = error.stack;
    if(/^\s+$/.test(stack)) {
      console.error(`No error stack was provided`);
    } else {
      console.error(stack);
    }
    console.error(`Build Failed`);
    process.exit(1);
  })
