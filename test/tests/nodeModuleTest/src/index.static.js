// @flow strict
import preval from 'preval.macro';

console.log(preval("console.log(\"PREVALLING\");"));

function mod(a: number, b: number): number {
  return a % b;
}

if(module.parent) {
  module.exports = mod;
} else {
  console.log(`mod(3, 4) = ${mod(3,4)}`);
}