import path from 'path';
import hashFileTree from 'hash-file-tree';
import run from '../run';

describe('nodeModuleTest', function() {
  it('should be able to generate a node module', async () => {
    expect.assertions(6);

    const directory = path.join(__dirname);

    let result;

    result = await hashFileTree(directory, {exclude_dir: "(__snapshots__|node_modules|dist)", exclude_file: "yarn.lock"});
    expect(result).toMatchSnapshot();
    
    result = await run(`yarn build`, { cwd: directory });
    expect(result.stderr).toBe("");
    expect(result.stdout).toEqual(expect.not.stringContaining("ERROR"));
    expect(result.stdout).toEqual(expect.not.stringContaining("WARNING"));

    result = await hashFileTree(path.join(directory, 'dist'));
    expect(result).toMatchSnapshot();

    result = await run(`node ./dist/index.js`, { cwd: directory });
    expect(result).toMatchSnapshot();
  }, 60000);
})