import path from 'path';
import hashFileTree from 'hash-file-tree';
import run from '../run';

describe('Test 1', function() {
  it('should take a simple context in package.json', async function() {
    expect.assertions(6);

    const directory = path.join(__dirname);
    let result

    result = await hashFileTree(directory, {exclude_dir: "(__snapshots__|node_modules|outputfiles)", exclude_file: "yarn.lock"});
    expect(result).toMatchSnapshot();

    result = await run(`yarn build`, { cwd: directory });
    expect(result.stderr).toBe("");
    expect(result.stdout).toEqual(expect.not.stringContaining("ERROR"));
    expect(result.stdout).toEqual(expect.not.stringContaining("WARNING"));

    result = await hashFileTree(directory, {exclude_dir: "(__snapshots__|node_modules)", exclude_file: "yarn.lock"});
    expect(result).toMatchSnapshot();

    result = await run(`ls -1 outputfiles/ | xargs -I {} node outputfiles/{}`, { cwd: directory }); 
    expect(result).toMatchSnapshot();

  }, 60000);
})