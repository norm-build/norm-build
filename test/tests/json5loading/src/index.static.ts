import jsondata from './jsondata'
import json5data from './json5data'
import jsondata2 from './jsondata2'
import json5data2 from './json5data2'

export default [
  json5data,
  jsondata,
  json5data2,
  jsondata2
]