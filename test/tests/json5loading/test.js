import path from 'path';
import hashFileTree from 'hash-file-tree';
import run from '../run';

describe('JSON5 Loading', function() {
  it('should load json with comments and json5 files, as well as a json5 tsconfig', async function() {
    expect.assertions(5);

    const directory = path.join(__dirname);
    let result

    result = await hashFileTree(directory, {exclude_dir: "(__snapshots__|node_modules|dist)", exclude_file: "yarn.lock"});
    expect(result).toMatchSnapshot();
    
    result = await run(`yarn build`, { cwd: directory });
    expect(result.stderr).toBe("");
    expect(result.stdout).toEqual(expect.not.stringContaining("ERROR"));
    expect(result.stdout).toEqual(expect.not.stringContaining("WARNING"));

    result = await hashFileTree(directory, {exclude_dir: "(__snapshots__|node_modules)", exclude_file: "yarn.lock"});
    expect(result).toMatchSnapshot();

  }, 60000);
})