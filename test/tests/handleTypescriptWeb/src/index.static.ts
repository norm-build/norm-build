import example from './example';

console.log(example());

// Handle nullish coalescing
let x: Object | null = null;
let y;
y = x ?? 2;
x = {};
y = x ?? 2;

// Handle optional chaining
let z: {y?: {x?: {v?: {u?: {t?: any}}}}} = {};
let a;
a = z?.y?.x?.v?.u?.t;