import { exec } from 'child_process';

export default function run(command, options = {}) {
  return new Promise((pass, fail) => {
    exec(command, options, (error, stdout, stderr) => {
      error ? fail(error) : pass({stdout: stdout, stderr: stderr});
    })
  })
}
