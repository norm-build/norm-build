document.onload = () => {

  Array.prototype.slice.call(document.querySelectorAll(".QuickLinks__Link")).map((Link: HTMLElement) => {
    const href = Link.attributes?.href?.value;
    if(href) {
      Link.onclick = () => {
        window.open(href)
      }
    }
  })
}