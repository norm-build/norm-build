// @flow strict

declare module 'filesystem-traverse' {
  declare export default function process<T>({
    directory: string,
    include_file?: string | RegExp,
    include_dir?: string | RegExp,
    exclude_file?: string | RegExp,
    exclude_dir?: string | RegExp,
  }): Promise<T>
}

declare module 'ncp' {
  declare export function ncp(
    source: string,
    destination: string,
    options: {
      filter?: RegExp,
      transform?: (read: ReadableStream, write: WritableStream) => void,
      clobber?: boolean,
      dereference?: boolean,
      stopOnErro?: boolean,
      errs?: ReadableStream
    },
    callback: (Error) => void
  ): void;
  declare export function ncp(
    source: string,
    destination: string,
    callback: (Error) => void
  ): void;
}

declare module 'rimraf' {
  declare export default function rimraf(
    f: string,
    opts: {
      // TODO https://www.npmjs.com/package/rimraf
    },
    callback: (Error) => void
  ): void;
  declare export default function rimraf(
    f: string,
    callback: (Error) => void
  ): void;
}

declare module 'argparse' {
  declare export class ArgumentParser {
    constructor(?{addHelp?:boolean}): ArgumentParser;
    addArgument(Array<string> | string, {
      action?: 'storeTrue',
      help?: string,
      nargs?: '+'
    }): void;
    parseArgs(Array<string>): mixed
  }
}

declare module 'enhanced-resolve' {
  declare export class NodeJsInputFileSystem {}
  declare export class CachedInputFileSystem {
    constructor(NodeJsInputFileSystem, number): CachedInputFileSystem;
  }
  declare export class ResolverFactory {
    static createResolver({
      fileSystem: CachedInputFileSystem,
      extensions: Array<string>
    }): {
      resolve({}, string, string, {}, (err: ?Error, string) => void): void
    }
  }
}

declare module 'webpack-merge' {
  declare export default function merge<T>(...Array<T>): T;
}

declare type JSONMember =
    | JSONObject
    | JSONArray
    | typeof undefined
    | null
    | number
    | string;

declare type JSONObject = { [string]: JSONMember };
declare type JSONArray = Array<JSONMember>;
declare type JSONType = JSONObject | JSONArray;
declare var JSON: {
  stringify(object: mixed, replacer?: null, spacing?: number): string;
  parse(string): JSONMember;
}