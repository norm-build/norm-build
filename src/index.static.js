// @flow strict

/**
 * This file bootstraps and wraps the buildsystem.
 * 
 * Wrapping:
 * 
 * When required it exports the simple api of:
 *   (statusFunction: (string) => void, ...args: Array<string>) => Promise<string>
 * Where the first argument is a status function, and subsequent arguments are
 * strings, equivalent to CLI arguments (see docs).
 *
 * When invoked directly, it provides its own status function, and passes the process
 * arguments on.
 */

import makeLog from './log';
const log = makeLog("index");

import build from './indexa';

declare var module: { exports: typeof build, parent: boolean }; // It isn't a boolean, but let us pretend

log(`Invoked directly: ${Boolean(!module.parent).toString()}`);

if(!module.parent) {
  // This is the parent, and is being used to run this module
  const args = process.argv.slice(2);
  if(!args.filter(a => a[0] !== '-').length) {
    args.push(process.cwd());
  }
  build((m) => console.log(m), {}, ...args)
    .then(function (m): void {
      console.log(m);
      console.log(`Done`);
    })
    .catch(function (error: Error): void {
      console.error(`An error occurred`);

      const message = error.toString();
      if(/^\s+$/.test(message)) {
        console.error(`No error message was provided`);
      } else {
        console.error(message);
      }
      const stack = error.stack;
      if(/^\s+$/.test(stack)) {
        console.error(`No error stack was provided`);
      } else {
        console.error(stack);
      }
      console.error(`Build Failed`);
      process.exit(1);
    })
} else {
  // This module has been required
  module.exports = build;
}