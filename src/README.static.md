# norm-build

A "simple" build system for js - web and node.

A wrapper round webpack that moves configuration to a declaritive format based on a simple entry in package.json and filenames.

## node usage example

For example, build a node package by setting this in your package.json:

```
// package.json
"build": {
  "settings": {
    "module": true
  },
  "node": {
    "implicit": true,
    "output": {
      "libraryTarget": "commonjs2"
    }
  }
}
```

And adding the "pre-suffix" `.static` to your entrypoints.

```
 |- package.json
 |- src
 |  |- index.static.ts
 |  |- first_file.static.js
 |  \- second_file.js
```

When you run `yarn build`, you'll get

```
 |- package.json
 |- src
 |  |- index.static.ts
 |  |- sheep.jpg
 |  |- first_file.static.js
 |  |- flock.static.jpg
 |  \- second_file.js
 \- dist
    |- index.js
    |- 2df1c6e9149b04608aa35287d2820da9.jpg
    |- flock.jpg
    |- first_file.js
    \- package.json
```

Which is a publishable module!

## web usage example

TODO