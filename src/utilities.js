// @flow strict
export function assert<T>(test: T, msg?: string): T  {
  if(!test) {
    throw new Error(msg || "Failed Assertion Test");
  } else {
    return test;
  }
}

export function absurd<T>(x: empty): T {
  throw new Error("Type matching incomplete, an absurd condition reached");
}