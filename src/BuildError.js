// @flow strict
export default class BuildError extends Error {
  name = 'BuildError';

  static fromError(error: Error, message: string, removeStackLines: number = 0) {
    const buildError = new BuildError(message);
    buildError.stack = [
      buildError.stack.split('\n')[0],
      ...error.stack.split('\n').slice(removeStackLines)
    ].join('\n');
    return buildError;
  }
};