// @flow strict
export class RefinementError extends Error {
  constructor(path: Array<string>, message: string) {
    super(`Expected ${path[0]}: ${path.slice(1).join('.')} - ${message}`);
  }
}
export function isRefinementError(v: mixed): boolean %checks {
  return v instanceof RefinementError;
}
export type RefinementFunction<T> = (path: Array<string>, v: mixed) => T | RefinementError;

export function attempt<T>(refine: RefinementFunction<T>, path: Array<string>, v: mixed, fallback: T): T {
  const result = refine(path, v);
  return isRefinementError(result) ? fallback : result;
}

export function refineAny<T>(...refinements: Array<RefinementFunction<T>>): RefinementFunction<T> {
  return function(path: Array<string>, v: mixed): T | RefinementError {
    const errors: Array<RefinementError> = [];
    for(let i=0; i<refinements.length; i++) {
      const result = refinements[i](path, v);
      if(isRefinementError(result)) {
        errors.push(result);
      } else {
        return result;
      }
    }
    return new RefinementError(path, `\n\t${errors.map(e => e.message).join("\n OR \n\t")}`);
  }
}

export function useRefine<T>(refine: RefinementFunction<T | RefinementError>): (path: Array<string>, v: mixed) => T {
  return (path, v) => {
    const result = refine(path, v);
    if(isRefinementError(result)) {
      throw result;
    } else {
      return result;
    }
  }
}