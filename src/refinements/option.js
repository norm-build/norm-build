// @flow strict
import { isRefinementError, RefinementError } from './core';
import type { RefinementFunction } from './core';

export function isOption(v: mixed): boolean %checks {
  return v === null || typeof v === "undefined"
}
/**
 * Returns the defined value of an option type v:?T or the fallback f:T
 */
export function option<T>(v: ?T, f: T): T {
  return isOption(v) ? f : v;
}
export function refineOption(path: Array<string>, v: mixed): ?RefinementError {
  return isOption(v) ? v : new RefinementError(path, `to be null or undefined`);
} 