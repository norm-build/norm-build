// @flow strict
import { isRefinementError, RefinementError } from './core';
import type { RefinementFunction } from './core';

export function isArray(v: mixed): boolean %checks {
  return Array.isArray(v);
}

export function refineArray(path: Array<string>, v: mixed): $ReadOnlyArray<mixed> | RefinementError {
  return Array.isArray(v) ? v : new RefinementError(path, `to be an Array`);
}

export function refineArrayOf<T>(refineValues: RefinementFunction<T | RefinementError>): (path: Array<string>, v: mixed) => Array<T> | RefinementError {
  return (path: Array<string>, v: mixed) => {
    const array = refineArray(path, v);
    if(isRefinementError(array)) {
      return array;
    }
    const arrayRefined: Array<T> = new Array(array.length);
    for(let i=0; i<array.length; i++) {
      const result = refineValues(path.concat(i.toString()), array[i]);
      if(isRefinementError(result)) {
        return result;
      } else {
        arrayRefined[i] = result;
      }
    }
    return arrayRefined;
  }
}
export function refineFromEnumArray<E>(...enumValues: Array<E>): (path: Array<string>, v: mixed) => RefinementError | E {
  return (path: Array<string>, v: mixed) => {
    const index = enumValues.indexOf(v);
    return index === -1 ? new RefinementError(path, `to be part of the enum "${enumValues.join('", "')}"`) : enumValues[index];
  }
}