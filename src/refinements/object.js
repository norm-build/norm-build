// @flow strict
import { isRefinementError, RefinementError } from './core';
import type { RefinementFunction } from './core';

export function isObject(v: mixed): boolean %checks {
  return typeof v === 'object' && v !== null
}
export function refineObject(path: Array<string>, v:mixed): {+[string]: mixed} | RefinementError {
  return isObject(v) ? v : new RefinementError(path, `to be an Object`);
}
export function refineObjectOf<K, V>(refineKeys: RefinementFunction<K | RefinementError>, refineValues: RefinementFunction<V | RefinementError>): (path: Array<string>, v: mixed) => {[K]: V} | RefinementError {
  return (path: Array<string>, v: mixed) => {
    const obj = refineObject(path, v);
    if(isRefinementError(obj)) {
      return obj;
    }
    const objKeys = Object.keys(obj);
    const objRefined: {[K]: V} = {};
    for(let i=0; i<objKeys.length; i++) {
      const resultKey = refineKeys(path.concat('$Keys'), objKeys[i]);
      if(isRefinementError(resultKey)) {
        return resultKey;
      }
      const resultValue = refineValues(path.concat(objKeys[i]), obj[objKeys[i]]);
      if(isRefinementError(resultValue)) {
        return resultValue;
      }
      objRefined[resultKey] = resultValue;
    }
    return objRefined;
  }
}