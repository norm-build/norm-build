// @flow strict
import { isRefinementError, RefinementError } from './core';
import type { RefinementFunction } from './core';

export function isString(v: mixed): boolean %checks {
  return typeof v === 'string';
}
export function refineString(path: Array<string>, v: mixed): string | RefinementError {
  return isString(v) ? v : new RefinementError(path, `to be a String`);
}

// False if string is ""
export function isEmptyString<T>(v: T): boolean %checks {
  return typeof v === 'string' && v !== "";
}