# Refinements

Guard unknown data with refinement calls. Flow-type compatible.

This is a flow compatible full pass composable refinement module, based on the premise that a refinement function is of the form `<T>(path: Array<string>, v: mixed): T | RefinementError`.

Here the unknown value `v` is refined to the type `T`. If the refinement fails a `RefinementError` is returned. The `path` argument is used to provide a better contextual understanding of where a refinement error occurred.

Refinement functions are commonly called via the provided `attempt` function. This is of the form `<T>(RefinementFunction<T>, path: Array<string>, v: mixed, fallback: T): T`.

Standard refinement functions and refinement utility functions are provided.