// @flow strict
import { isRefinementError, RefinementError } from './core';
import type { RefinementFunction } from './core';

export function isBoolean(v: mixed): boolean %checks {
  return typeof v === 'boolean';
}
export function refineBoolean(path: Array<string>, v: mixed): boolean | RefinementError {
  return isBoolean(v) ? v : new RefinementError(path, `to be a boolean`);
}