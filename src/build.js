// @flow strict
import webpack from 'webpack';
import type { WebpackOptions, Stats } from 'webpack';
import tasks from './tasks';
import fs from 'fs';
import path from 'path';
import BuildError from './BuildError';
import { ncp } from 'ncp';
import traverse from 'filesystem-traverse';
import rimraf from 'rimraf';
import puppeteer from 'puppeteer';
import { isObject, isArray, option } from './refinements';
import JSON5 from 'json5';
import prettier from 'prettier';

import type { Page } from 'puppeteer';
import type { BuildConfig } from './buildSettings/BuildConfig.types';

async function removeFileIndirection(builtFile, outputContext) {
    // there exists a file that we want, named after its contenthash
    // the built file references it. We must replace the built file with
    // this contenthash file

    // This is done by scanning the builtFile (which we know is an indirection)
    //  for what looks like the target filename, then replacing it with that.
    // This is brittle in the case that the way the filename is specified changes
    //  or that the boilerplate starts to contain a 32 character hex string that
    //  could be mistaken for the target file.
    // An applicative approach is complex and not much more resistant. It might be
    //  better to scan to see that the target file exists and fall back to the next
    //  one however - but this seems unlikely.
    
    // $ExpectError: flowlib has the wrong type (expects Buffer)
    const data = await fs.promises.readFile(outputContext(builtFile), { encoding: 'utf8' });
    const generatedFilenameRegex = /"[0-f]{32}[^"]+"/;
    const possibleFilenames = data.match(generatedFilenameRegex);
    if(possibleFilenames === null || possibleFilenames.length === 0) {
      throw new BuildError(`Failed to find a generated filename for the indirection of ${builtFile}`)
    }
    const generatedFile = possibleFilenames[0].slice(1, -1);
    await fs.promises.rename(outputContext(generatedFile), outputContext(builtFile));
}

function sleep(amount: number): Promise<void> {
  return new Promise((pass) => setTimeout(pass, amount));
}

async function waitForStability(page: Page): Promise<string> {
  const timeout = 5000;
  const interval = 10;

  const start = new Date().getTime();
  const factor = 1.2; // Backoff factor, must be > 1, test gradually less frequently
  const increment = 10;
  
  let last = await page.content();
  await sleep(interval);
  let next: string = await page.content();

  let delay;
  for(let i=1; last !== next; i++) {
    if(new Date().getTime() - start > timeout) {
      throw new Error(`Target did not stabilise within ${timeout}ms`)
    }

    delay = Math.min(increment * factor * i, 2000); // backoff
    delay = Math.max(delay, increment); // minimum interval
    await sleep(delay);

    last = next;
    next = await page.content();
  }
  return next;
}

async function renderFile(builtFile, outputContext) {
  const browser = await puppeteer.launch({
    args: ["--headless", "--no-sandbox" ]
  });
  const page = await browser.newPage();
  const target = "file://" + path.join(process.cwd(), outputContext(builtFile));
  await page.goto(target, { waitUntil: "networkidle0" });
  let data = await waitForStability(page);
  data = prettier.format(data, { parser: "html"});
  await fs.promises.writeFile(outputContext(builtFile), data);
  await fs.promises.rename(outputContext(builtFile), outputContext(builtFile).replace(/\.render\.html$/, '.html'))
  await browser.close();
}

async function applyFile(builtFile, outputContext) {
  // the built file needs applied to generate the output file that will replace it
  //  but its easier to just "grab" it
  const data = await fs.promises.readFile(outputContext(builtFile), { encoding: 'utf8'});
  const startString = "__webpack_exports__[\"default\"] = (\"";
  let start = data.indexOf(startString);
  if(start === -1) {
    throw new Error("Unable to find start of export")  
  }
  start = start + startString.length;
  const endString = "\");\n";
  const end = data.indexOf(endString, start);
  if(end === -1) {
    throw new Error("Unable to find end of export")  
  }
  const encodedData = data.slice(start, end);
  // $ExpectError: Hacky way to ensure it is parsed properly(ish)
  const actualData: string = JSON.parse(`"${encodedData}"`);
  await fs.promises.writeFile(outputContext(builtFile), actualData);
}

async function extractJson5(builtFile, outputContext) {
  // Similar to apply test, but a different mechanism
  const data = await fs.promises.readFile(outputContext(builtFile), { encoding: 'utf8'});
  const startString = "module.exports = ";
  let start = data.indexOf(startString);
  if(start === -1) {
    throw new Error("Unable to find start of export")  
  }
  start = start + startString.length;
  const endString = "\n";
  const end = data.indexOf(endString, start);
  if(end === -1) {
    throw new Error("Unable to find end of export")  
  }
  const encodedData = data.slice(start, end);
  // $ExpectError: Hacky way to ensure it is parsed properly(ish)
  const actualData: string = JSON.parse(`"${encodedData}"`);
  await fs.promises.writeFile(outputContext(builtFile), actualData);
}

async function extractJson(builtFile, outputContext) {
  // Similar to apply test, but a different mechanism
  const data = await fs.promises.readFile(outputContext(builtFile), { encoding: 'utf8'});
  const startString = "module.exports = JSON.parse(\"";
  let start = data.indexOf(startString);
  if(start === -1) {
    throw new Error("Unable to find start of export")  
  }
  start = start + startString.length;
  const endString = "\");\n";
  const end = data.indexOf(endString, start);
  if(end === -1) {
    throw new Error("Unable to find end of export")  
  }
  const encodedData = data.slice(start, end);
  // $ExpectError: Hacky way to ensure it is parsed properly(ish)
  const actualData: string = JSON.parse(`"${encodedData}"`);
  await fs.promises.writeFile(outputContext(builtFile), actualData);
}

function recursiveCopy(source: string, destination: string): Promise<void> {
  return new Promise((pass, fail) => 
    ncp(source, destination, (err) => err ? fail(err) : pass())
  )
}

function recursiveRemove(dir: string): Promise<void> {
  return new Promise((pass, fail) => 
    rimraf(dir, (err) => err ? fail(err) : pass())
  )
}

function webpackPromise(tasks: Array<WebpackOptions>): Promise<Stats> {
  return new Promise((pass, fail) => {
    webpack(tasks, (err, stats) => err ? fail(err) : pass(stats))
  })
}

function replaceLast(find, replace, string) {
  var lastIndex = string.lastIndexOf(find);
  if(lastIndex === -1)
    return string;
  const beginString = string.substring(0, lastIndex);
  const endString = string.substring(lastIndex + find.length);
  return beginString + replace + endString;
}

async function preWebpack(config: BuildConfig): Promise<void> {
  // Create a temporary directory to use as the source and dest

  // I tried using temporary directories that contained the creation date and a random string in them
  //  in order to allow parallel builds. This did not go well as the hashed filenames that webpack was
  //  creating started to contain this per build random infomation! It prevented the builds from being
  //  duplicable.
  // This does not mean such an attempt will not work, in fact it has worked before, however the paths
  //  in config were normalised to remove a different pathing issue which broke this. As this has been
  //  a messy ride, I'll come back to parallel builds another time.
  // n.b. the temp_entry *must* be placed as a sibling of the real entry so that relative includes work
  //  fine. 
  const [temp_entry, temp_output] = [ "in", "out"].map(s => `norm-temp-${s}`);

  let real_context = config.context;
  config.real_context = real_context;
  config.context = {
    output: './' + path.join(
      './',
      real_context.output
        .split(path.sep)
        .slice(0, -1)
        .concat([temp_output])
        .join(path.sep)
    ),
    entry: './' + path.join(
      './',
      real_context.entry
        .split(path.sep)
        .slice(0, -1)
        .concat([temp_entry])
        .join(path.sep)
    ),
  };

  if(config.build) {
    const build = config.build;
    if(build.node) {
      let nodeEntry = build.node.entry;
      build.node.entry = Object.keys(nodeEntry).reduce((final, key) => {
        const file_rel = path.relative(config.real_context.entry, nodeEntry[key]);
        const rel = './'+path.join(config.context.entry, file_rel);
        final[key] = rel;
        return final;
      }, {});
      build.node.output.path = config.context.output;
    }
    if(build.web) {
      let webEntry = build.web.entry;
      build.web.entry = Object.keys(webEntry).reduce((final, key) => {
        const file_rel = path.relative(config.real_context.entry, webEntry[key]);
        const rel = './'+path.join(config.context.entry, file_rel);
        final[key] = rel;
        return final;
      }, {});
      build.web.output.path = config.context.output;
    }
  }

  await recursiveCopy(real_context.entry, config.context.entry);

  const typescriptFiles = (await traverse({
    directory: config.context.entry,
    include_file: /.*\.ts$/,
    process_file: () => true,
  })).length > 0;

  if(typescriptFiles) {
    // Make a copy of all .static files without static so ts-loader doesn't cry so much
    await Promise.all(await traverse({
      directory: config.context.entry,
      include_file: /.*\.static\.([a-zA-Z0-9]+|d.\.tsx?)$/,
      process_file: (filepath) => {
        let o = path.join(config.context.entry, filepath);
        let n = path.join(config.context.entry, replaceLast('.static', '', filepath));
        let p = o.split(path.sep).slice(-1)[0].split('.').slice(0, -1).join('.');
        return fs.promises.writeFile(n, `export * from './${p}';\nexport { default } from './${p}';\n`);
      }
    }))

    // Setup a tsconfig
    const tsconfigpath = path.join(config.context.entry, 'tsconfig.json');
    const declarationDir = path.relative(config.context.entry, config.context.output) + "/";
    const outDir = path.relative(config.context.entry, config.context.output) + "/";
    let tsconfig =  
      {
        "compilerOptions": {
          "jsx": "preserve",
          "declaration": true,
          declarationDir,
          outDir,
          "esModuleInterop": true,
          "allowSyntheticDefaultImports": true,
          "resolveJsonModule": true
        },
        "include": [`${path.resolve(config.context.entry)}/**/*`]
      };

    // Read their tsconfig file and extend it
    if(fs.existsSync(tsconfigpath)) {
      // $ExpectError: fs.promises.readFile with encoding set to utf-8 will always produce a string
      let loadedjson = JSON5.parse(await fs.promises.readFile(tsconfigpath, { encoding: 'utf-8' }));
      let usertsconfig = isObject(loadedjson) && !isArray(loadedjson) && option(loadedjson) || {};
      let usercompileroptions = !isArray(usertsconfig.compilerOptions) && option(usertsconfig.compilerOptions) || {};
      let userinclude = isArray(usertsconfig.include) && option(usertsconfig.include) || [];
      
      tsconfig = {
        ...usertsconfig,
        ...tsconfig,
        compilerOptions: {
          ...usercompileroptions,
          ...tsconfig.compilerOptions,
        },
        include: tsconfig.include.concat(userinclude)
      }
    }

    // Write a tsconfig file
    await fs.promises.writeFile(tsconfigpath, JSON.stringify(tsconfig))

    // Setup globals
    const tsglobalspath = path.join(config.context.entry, 'globals.d.ts');
    const tsglobals = "\ndeclare module '*.md';\ndeclare module '*.json5';\n"
    if(fs.existsSync(tsglobalspath)) {
      await fs.promises.appendFile(tsglobalspath, tsglobals, 'utf8');
    } else {
      await fs.promises.writeFile(tsglobalspath, tsglobals, 'utf8');
    }
  }
}

import ReplaceStream from '@normed/replace-stream';

async function postWebpack(config: BuildConfig) {
  const real_context = config.real_context;
  if(!real_context)
    throw new Error("Missing real context")
  
  await recursiveRemove(real_context.output);
  await fs.promises.rename(path.resolve(config.context.output), path.resolve(real_context.output));
  await recursiveRemove(config.context.entry);
}

async function applyImplicit(configs: Array<BuildConfig>) {
  // De-indirect files loaded with file-loader
  // Apply files loaded with extract loader
  const indirectTest = /\.(png|html|jpg|eot|woff|ttf|svg|md)$/;
  const applyTest = /\.(pug|less|css)$/;
  const jsonTest = /\.(json)$/;
  const json5Test = /\.(json5)$/;
  
  const renderTest = /\.render\.(pug|html)$/;
  
  await Promise.all(configs.map(async config => {
    if(!config.implicit) {
      return;
    }
    
    const implicit = config.implicit;
    
    function outputContext(x) {
      return path.join("./", config.context.output, x);
    }

    await Promise.all(Object.keys(implicit).map(async (builtFile) => {
      const originalFile = implicit[builtFile];
      if(indirectTest.test(originalFile)) {
        try {
          await removeFileIndirection(builtFile, outputContext);
        } catch (e) {
          throw BuildError.fromError(e, `Failed to remove file indirection of ${builtFile}: ${e.message}`, 1);
        }
      } else if(applyTest.test(originalFile)) {
        try {
          await applyFile(builtFile, outputContext)
        } catch(e) {
          throw BuildError.fromError(e, `Failed to apply ${builtFile}: ${e.message}`, 1);
        }
      } else if(jsonTest.test(originalFile)) {
        try {
          await extractJson(builtFile, outputContext);
        } catch(e) {
          throw BuildError.fromError(e, `Failed to extract JSON from ${builtFile}: ${e.message}`, 1);
        }
      } else if(json5Test.test(originalFile)) {
        try {
          await extractJson5(builtFile, outputContext);
        } catch(e) {
          throw BuildError.fromError(e, `Failed to extract JSON5 from ${builtFile}: ${e.message}`, 1);
        }
      }

      if(renderTest.test(originalFile)) {
        try {
          await renderFile(builtFile, outputContext);
        } catch(e) {
          throw BuildError.fromError(e, `Failed to render ${builtFile}: ${e.message}`, 1);
        }
      }
    }))
  }))
}

export default (statusFunction: (string | Error) => void) =>
  async (configs: Array<BuildConfig>): Promise<string | Error | void> => {
    const watch = configs.some(config => config.settings.watch);

    await Promise.all(configs.map(preWebpack));
    const allTasks = tasks(configs).map((task, index) => {
      let output = task.output;
      if(typeof output === 'object' && output !== null)
        output.path = path.resolve(configs[index].context.output);
      return task;
    });

    try {
      const stats = await webpackPromise(allTasks)
      
      // $ExpectError: the types for Stats.toString are contradictory for the option colors
      const message = stats.toString({
        assets: true,
        cached: true,
        children: true,
        chunks: true,
        chunkModules: false,
        colors: true,
        modules: false,
        timings: true,
        version: true,
        warnings: true,
      })
      
      await applyImplicit(configs)
      await Promise.all(configs.map(postWebpack));
    
      if(watch) {
        statusFunction(message);
      } else {
        return message;
      }
    } catch(e) {
      if(watch) {
        statusFunction(e);
      } else {
        throw e;
      }
    }
  }
