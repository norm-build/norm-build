// @flow strict

import {
  NodeJsInputFileSystem,
  CachedInputFileSystem,
  ResolverFactory
} from 'enhanced-resolve';

const resolver: (string) => Promise<string> = (() => {
  const Resolver = ResolverFactory.createResolver({
    fileSystem: new CachedInputFileSystem(new NodeJsInputFileSystem(), 4000),
    extensions: ['.js', '.json']
  });
  return function(request: string): * {
    return new Promise<string>(function(pass, fail): void {
      Resolver.resolve({}, __dirname, request, {}, function(err, path) {
        err ? fail(err) : pass(path);
      })
    });
  }
})();

export default resolver;