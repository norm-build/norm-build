// @flow strict
/*

  Goes through all the paths in the BuildConfig and makes sure they are
  relative to the cwd.

**/
import makeLog from '../log';
const log = makeLog();

import type { Context, Entries, BuildConfig, Settings } from './BuildConfig.types';
import path from 'path';
import { option } from '../refinements';

/*
  Takes an entries object and adds the context paths to each entry
*/
function contextualiseEntries(context: Context, ...objs: Array<Entries>): Entries {
  const merged = Object.assign(({}: Entries), ...objs);
  return Object.keys(merged).reduce((acc, v) => {
    acc[v] = "./" + path.join(context.entry, merged[v]);
    return acc;
  }, {})
}

function clone<T>(i: T): T {
  // $ExpectError: Hacked in clone, not perfect (wrt. classes)
  return JSON.parse(JSON.stringify({v: i})).v;
}

/*
  Takes the build object and adds the context paths to each entry and the output
*/
function contextualise(buildPart, context, modulePath) {
  const output = buildPart.output || {};
  return {
    ...buildPart,
    entry: contextualiseEntries(context, buildPart.entry || {}),
    output: {
      ...buildPart.output,
      filename: option(output.filename, "[name]"),
      path: path.join(modulePath.split("/").slice(0, -1).join("/"), context.output, output.path || "")
    }
  }
}

/*
  Takes a BuildConfig containing at least a settings and build part, and outputs the same, but with
  the build part contextualised.
 */
export function contextualisePartialBuildConfig(config: BuildConfig, modulePath: string): BuildConfig {
  const outputBuild = clone(config.build);
  if(outputBuild.web) {
    outputBuild.web = contextualise(outputBuild.web, config.context, modulePath)
  }
  if(outputBuild.node) {
    outputBuild.node = contextualise(outputBuild.node, config.context, modulePath)
  }
  return {
    ...config,
    build: outputBuild
  };
}