// @flow strict
import makeLog from '../log';
const log = makeLog("buildSettings"); 

import useRefinePackageJson from './refinePackageJson';
import type { Build, Context, BuildConfig, Settings, BuildType } from './BuildConfig.types';
import { contextualisePartialBuildConfig } from './contextualise';
import { absurd } from '../utilities';
import { option, isArray, isObject } from '../refinements';
import path from 'path';
import fs from 'fs';
import { promisify } from 'util';
const readFile: (string, 'utf8') => Promise<string> = promisify(fs.readFile);
import resolve from './resolve';
import { getImplicitFiles, implicitType } from './implicitFiles';

async function readModule(module: string): JSONMember {
  const filename = await resolve(module);
  const data = await readFile(filename, 'utf8');
  return JSON.parse(data);
}

function remove(key, obj) {
  const o = {...obj};
  if(o.hasOwnProperty(key))
    // $ExpectError: I don't get it "Cannot delete `o[key]` because property `implicit` is missing in  object literal [1].Flow(InferError)"
    delete o[key];
  return o;
}

async function readConfigFromModule(module: string): Promise<BuildConfig> {
  log(`buildSettings/index.js: Parsing module: ${module}`);
  const modulePath = path.join(module, 'package.json');
  
  const data: JSONMember = await readModule(modulePath);
  log(`buildSettings/index.js: Read module: ${JSON.stringify(data, null, 2)}`);
  let partialBuildConfig: BuildConfig = {
    ...useRefinePackageJson([modulePath], (data: mixed)),
    modulePath: module
  };
  log(`buildSettings/index.js: non-contextual refined partialBuildConfig: ${JSON.stringify(partialBuildConfig, null, 2)}`);
  partialBuildConfig = contextualisePartialBuildConfig(partialBuildConfig, modulePath);
  log(`buildSettings/index.js: non-implicit partialBuildConfig: ${JSON.stringify(partialBuildConfig, null, 2)}`);

  const type = implicitType(partialBuildConfig.build);
  log(`buildSettings/index.js: implicitType ${JSON.stringify(type)}`);

  if(type !== null) {
    const implicit = await getImplicitFiles(partialBuildConfig.context, modulePath);
    log(`buildSettings/index.js: implcitiFiles: ${JSON.stringify(implicit, null, 2)}`);
    return {
      ...partialBuildConfig,
      build: {
        ...partialBuildConfig.build,
        ...((): {
          web?: BuildType,
          node?: BuildType
        } => {
          const config: BuildType = {
            ...remove("implicit", partialBuildConfig.build[type]), // Build becomes a webpack task, and implicit is not a valid key
            entry: {
              ...implicit,
              // $ExpectError: wrt flow error: flow seems to think partialBuildConfig is any despite being annotated
              ...option<{[string]: string}>(partialBuildConfig.build[type], {}).entry
            }
          }
          switch (type) {
            case "node":
              return {
                node: config
              }
            case "web":
              return {
                web: config
              }
            default:
              return absurd(type)
          }
        })(),
      },
      implicit
    };
  }

  return partialBuildConfig;
}

export default async function buildSettings(modules: Array<string>): Promise<Array<BuildConfig>> {
  log(`buildSettings/index.js: args: ${JSON.stringify(modules, null, 2)}`);
  return await Promise.all(modules.map(readConfigFromModule));
}