// @flow strict
export type Settings = {
  watch?: boolean,
  analyse?: boolean,
  production?: boolean,
  packageJson?: {
    name?: string,
    version?: string,
    description?: string,
    keywords?: Array<string>,
    license?: string,
    homepage?: string,
    bugs?: string,
    repository?: string | {
      type: string,
      url: string
    },
    author?: string | {
      name: string,
      email: string,
      url: string
    },
    contributors?: Array<string> | Array<{
      name: string,
      email: string,
      url: string
    }>,
    files?: Array<string>,
    main?: string,
    bin?: string | {[string]: string},
    man?: string | Array<string>,
    directories?: {[string]: string},
    dependencies?: {[string]: string},
    devDependencies?: {[string]: string},
    peerDependencies?: {[string]: string},
    optionalDependencies?: {[string]: string},
    bundleDependencies?: Array<string>,
    flat?: boolean,
    resolutions?: {[string]: string},
    engines?: {[string]: string},
    os?: Array<string>,
    cpu?: Array<string>,
    private?: boolean
  },
  executable?: boolean,
  module?: boolean,
  modules?: Array<string>
};

export type Path = string;
export type Entries = {[Path]: Path};
export type BuildType_output = {
  filename?: string,
  path?: string,
  publicPath?: string,
  library?: string,
  libraryTarget?: string,
}
export type BuildType = {
  implicit?: true,
  entry: {[string]: string},
  output?: BuildType_output
}
export type Build = {
  web?: BuildType,
  node?: BuildType,
};
export type Context = {
  entry: string,
  output: string,
}
export type BuildConfig = {
  build: Build,
  implicit?: {[Path]: Path},
  context: Context,
  real_context?: Context, // Long lived temporary option for hacking around ts-loader, check usage or ignore
  modulePath: Path,
  settings: Settings
};