// @flow strict

/**
  Finds the implict files (** /*.static.*) and creates a mapping
  of {dest: src}
**/
import traverse from 'filesystem-traverse';
import { option, isOption, isEmptyString } from '../refinements';
import type {Path, Context, Build, Entries} from './BuildConfig.types';
import { exec } from 'child_process';
import * as path from 'path';
import { writeFile } from 'fs';
import makeLog from '../log';
const log = makeLog();

// Replace the last instance of a substring v in a string str
function replaceLast(str: string, v: string): string {
  const i = str.lastIndexOf(v);
  const i2 = i + v.length;
  return `${str.slice(0, i)}${str.slice(i2)}`;
}

// Identify the implicit files for the provided entry
export async function getImplicitFiles(context: Context, modulePath: string): Entries {
  const route = [...modulePath.split(path.sep).slice(0, -1), ...context.entry.split(path.sep)].join(path.sep);
  const staticFiles: Array<Path> = await traverse({
    process_file(filepath: string): Path {
      log("Processing file:", filepath);
      return filepath;
    },
    directory: route,
    include_file: /.*\.static\.[^\/]+$/
  });
  const data = staticFiles.reduce((acc, f) => {
    const targetFile = path.join(replaceLast(f, '.static'))
      .replace(/\.pug$/, '.html')
      .replace(/\.less$/, '.css')
      .replace(/\.(j|t)sx?$/, '.js')
      ;
    acc[targetFile] = `./${path.join('.', context.entry, f)}`;
    return acc;
  }, ({}: Entries));
  return data;
}

// Determine which build type is implicit, if any
export function implicitType(build: Build): "web" | "node" | null {
  const implicitWeb = build && build.web && build.web.implicit && "web";
  const implicitNode = build && build.node && build.node.implicit && "node";
  const implicit = implicitWeb || implicitNode;

  if(implicitNode && implicitWeb) {
    throw new Error("Only one config entry, node or web, can specify implicit");
  }

  return implicit || null;
}