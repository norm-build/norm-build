// @flow strict
import makeLog from '../log';
const log = makeLog("refinePackageJson");
import {
  isObject,
  option,
  refineAny,
  refineOption,
  isRefinementError,
  refineBoolean,
  refineObject,
  refineString,
  isOption,
  refineObjectOf,
  refineFromEnumArray,
  refineArrayOf,
  RefinementError,
  attempt,
  useRefine
} from '../refinements';

import mergeSettings from '../mergeSettings';

import type {
  BuildConfig,
  BuildType_output,
  BuildType,
  Build,
  Context,
  Path,
  Settings
} from './BuildConfig.types.js';

const defaultSettings: Settings = {
  modules: []
}

function refineSettingsPackageJson(path: Array<string>, v: mixed): $PropertyType<Settings, "packageJson"> | RefinementError {
  const obj = refineObject(path, v);
  if(isRefinementError(obj)) {
    return obj;
  }
  
  let _package: $PropertyType<Settings, "packageJson"> = {
  }

  if(!isOption(obj.name)) {
    const name = refineString(path.concat('name'), obj.name);
    if(isRefinementError(name)) {
      return name;
    }
    _package = {
      ..._package,
      name
    }
  }
  if(!isOption(obj.version)) {
    const version = refineString(path.concat('version'), obj.version);
    if(isRefinementError(version)) {
      return version;
    }
    _package = {
      ..._package,
      version
    }
  }


  if(!isOption(obj.description)) {
    const description = refineString(path.concat('description'), obj.description);
    if(isRefinementError(description)) {
      return description;
    } else {
      _package = {
        ..._package,
        description
      };
    }
  }
  if(!isOption(obj.keywords)) {
    const keywords = refineArrayOf(refineString)(path.concat('keywords'), obj.keywords);
    if(isRefinementError(keywords)) {
      return keywords;
    } else {
      _package = {
        ..._package,
        keywords
      };
    }
  }
  if(!isOption(obj.license)) {
    const license = refineString(path.concat('license'), obj.license);
    if(isRefinementError(license)) {
      return license;
    } else {
      _package = {
        ..._package,
        license
      };
    }
  }
  if(!isOption(obj.homepage)) {
    const homepage = refineString(path.concat('homepage'), obj.homepage);
    if(isRefinementError(homepage)) {
      return homepage;
    } else {
      _package = {
        ..._package,
        homepage
      };
    }
  }
  if(!isOption(obj.bugs)) {
    const bugs = refineString(path.concat('bugs'), obj.bugs);
    if(isRefinementError(bugs)) {
      return bugs;
    } else {
      _package = {
        ..._package,
        bugs
      };
    }
  }
  if(!isOption(obj.repository)) {
    function refineRepository(path: Array<string>, v: mixed): {type: string, url: string} | RefinementError {
      const obj = refineObject(path, v);
      if(isRefinementError(obj)) {
        return obj;
      }
      const type = refineString(path.concat('type'), obj.type);
      if(isRefinementError(type)) {
        return type;
      }
      const url = refineString(path.concat('url'), obj.url);
      if(isRefinementError(url)) {
        return url;
      }
      return {
        type,
        url
      }
    }
    const repository = refineAny(refineString, refineRepository)(path.concat('repository'), obj.repository);
    if(isRefinementError(repository)) {
      return repository;
    } else {
      _package = {
        ..._package,
        repository
      };
    }
  }
  type Author = {name: string, email: string, url: string}
  function refineAuthor(path: Array<string>, v: mixed): Author | RefinementError {
    const obj = refineObject(path, v);
    if(isRefinementError(obj)) {
      return obj;
    }
    const name = refineString(path.concat('name'), obj.name);
    if(isRefinementError(name)) {
      return name;
    }
    const email = refineString(path.concat('email'), obj.email);
    if(isRefinementError(email)) {
      return email;
    }
    const url = refineString(path.concat('url'), obj.url);
    if(isRefinementError(url)) {
      return url;
    }
    return {
      name,
      email,
      url
    }
  }
  if(!isOption(obj.author)) {
    log(`Author: ${JSON.stringify(obj.author)}`);
    const author = refineAny(refineString, refineAuthor)(path.concat('author'), obj.author);
    if(isRefinementError(author)) {
      return author;
    } else {
      _package = {
        ..._package,
        author
      };
    }
  }

  if(!isOption(obj.contributors)) {
    const contributors = refineAny(
      refineArrayOf(refineString),
      refineArrayOf<Author>(refineAuthor)
    )(path.concat('contributors'), obj.contributors);
    if(isRefinementError(contributors)) {
      return contributors;
    } else {
      _package = {
        ..._package,
        contributors
      };
    }
  }
  
  if(!isOption(obj.files)) {
    const files = refineArrayOf(refineString)(path.concat('files'), obj.files);
    if(isRefinementError(files)) {
      return files;
    } else {
      _package = {
        ..._package,
        files
      };
    }
  }
  
  if(!isOption(obj.main)) {
    const main = refineString(path.concat('main'), obj.main);
    if(isRefinementError(main)) {
      return main;
    } else {
      _package = {
        ..._package,
        main
      };
    }
  }
  if(!isOption(obj.bin)) {
    const bin = refineAny(
      refineString,
      refineObjectOf(refineString, refineString)
    )(path.concat('bin'), obj.bin);
    if(isRefinementError(bin)) {
      return bin;
    } else {
      _package = {
        ..._package,
        bin
      };
    }
  }
  if(!isOption(obj.man)) {
    const man = refineAny(
      refineString,
      refineArrayOf(refineString)
    )(path.concat('man'), obj.man);
    if(isRefinementError(man)) {
      return man;
    } else {
      _package = {
        ..._package,
        man
      };
    }
  }
  if(!isOption(obj.directories)) {
    const directories = refineObjectOf(refineString, refineString)(path.concat('directories'), obj.directories);
    if(isRefinementError(directories)) {
      return directories;
    } else {
      _package = {
        ..._package,
        directories
      };
    }
  }
  if(!isOption(obj.dependencies)) {
    const dependencies = refineObjectOf(refineString, refineString)(path.concat('dependencies'), obj.dependencies);
    if(isRefinementError(dependencies)) {
      return dependencies;
    } else {
      _package = {
        ..._package,
        dependencies
      };
    }
  }
  /* We probably don't want to copy over devDependencies. They are useful for developing the package,
   * but aren't part of the production package. Whilst it may do no harm, it may cause diffs when there
   * is no need.
   */
  //if(!isOption(obj.devDependencies)) {
  //  const devDependencies = refineObjectOf(refineString, refineString)(path.concat('devDependencies'), obj.devDependencies);
  //  if(isRefinementError(devDependencies)) {
  //    return devDependencies;
  //  } else {
  //    _package = {
  //      ..._package,
  //      devDependencies
  //    };
  //  }
  //}
  if(!isOption(obj.peerDependencies)) {
    const peerDependencies = refineObjectOf(refineString, refineString)(path.concat('peerDependencies'), obj.peerDependencies);
    if(isRefinementError(peerDependencies)) {
      return peerDependencies;
    } else {
      _package = {
        ..._package,
        peerDependencies
      };
    }
  }
  if(!isOption(obj.optionalDependencies)) {
    const optionalDependencies = refineObjectOf(refineString, refineString)(path.concat('optionalDependencies'), obj.optionalDependencies);
    if(isRefinementError(optionalDependencies)) {
      return optionalDependencies;
    } else {
      _package = {
        ..._package,
        optionalDependencies
      };
    }
  }
  if(!isOption(obj.bundleDependencies)) {
    const bundleDependencies = refineArrayOf(refineString)(path.concat('bundleDependencies'), obj.bundleDependencies);
    if(isRefinementError(bundleDependencies)) {
      return bundleDependencies;
    } else {
      _package = {
        ..._package,
        bundleDependencies
      };
    }
  }
  if(!isOption(obj.flat)) {
    const flat = refineBoolean(path.concat('flat'), obj.flat);
    if(isRefinementError(flat)) {
      return flat;
    } else {
      _package = {
        ..._package,
        flat
      };
    }
  }
  if(!isOption(obj.resolutions)) {
    const resolutions = refineObjectOf(
      refineString,
      refineString
    )(path.concat('resolutions'), obj.resolutions);
    if(isRefinementError(resolutions)) {
      return resolutions;
    } else {
      _package = {
        ..._package,
        resolutions
      };
    }
  }
  if(!isOption(obj.engines)) {
    const engines = refineObjectOf(
      refineString,
      refineString
    )(path.concat('engines'), obj.engines);
    if(isRefinementError(engines)) {
      return engines;
    } else {
      _package = {
        ..._package,
        engines
      };
    }
  }
  if(!isOption(obj.cpu)) {
    const cpu = refineArrayOf(
      refineString
    )(path.concat('cpu'), obj.cpu);
    if(isRefinementError(cpu)) {
      return cpu;
    } else {
      _package = {
        ..._package,
        cpu
      };
    }
  }
  if(!isOption(obj.os)) {
    const os = refineArrayOf(
      refineString
    )(path.concat('os'), obj.os);
    if(isRefinementError(os)) {
      return os;
    } else {
      _package = {
        ..._package,
        os
      };
    }
  }
  if(!isOption(obj.private)) {
    const _private = refineBoolean(path.concat('_private'), obj.private);
    if(isRefinementError(_private)) {
      return _private;
    } else {
      _package = {
        ..._package,
        private: _private
      };
    }
  }
  
  return _package;
}

function refineSettings(path: Array<string>, v: mixed): Settings | RefinementError {
  const obj = refineObject(path, v);
  if(isRefinementError(obj)) {
    return obj;
  }

  let settings: Settings = {};

  if(!isOption(obj.watch)) {
    const watch = refineBoolean(path.concat('watch'), obj.watch);
    if(isRefinementError(watch)) {
      return watch;
    }
    settings = {
      ...settings,
      watch
    }
  }
  if(!isOption(obj.analyse)) {
    const analyse = refineBoolean(path.concat('analyse'), obj.analyse);
    if(isRefinementError(analyse)) {
      return analyse;
    }
    settings = {
      ...settings,
      analyse
    }
  }
  if(!isOption(obj.executable)) {
    const executable = refineBoolean(path.concat('executable'), obj.executable);
    if(isRefinementError(executable)) {
      return executable;
    }
    settings = {
      ...settings,
      executable
    }
  }
  if(!isOption(obj.module)) {
    const module = refineBoolean(path.concat('module'), obj.module);
    if(isRefinementError(module)) {
      return module;
    }
    settings = {
      ...settings,
      module
    }
  }
  if(!isOption(obj.production)) {
    const production = refineBoolean(path.concat('production'), obj.production);
    if(isRefinementError(production)) {
      return production;
    }
    settings = {
      ...settings,
      production
    }
  }
  if(!isOption(obj.modules)) {
    const modules = refineArrayOf(refineString)(path.concat('modules'), obj.modules);
    if(isRefinementError(modules)) {
      return modules;
    }
    settings = {
      ...settings,
      modules
    }
  }
  if(!isOption(obj.packageJson)) {
    const packageJson = refineSettingsPackageJson(path.concat('packageJson'), obj.packageJson);
    if(isRefinementError(packageJson)) {
      return packageJson;
    }
    settings = {
      ...settings,
      packageJson
    }
  }

  return settings;
}

function refinePackageJsonToSettings(path: Array<string>, v: mixed): Settings | RefinementError {
  const obj = attempt(refineObject, path, v, {});
  if(isRefinementError(obj)) {
    return obj;
  }

  const primary_packageJson = refineSettingsPackageJson(path, obj);
  if(isRefinementError(primary_packageJson)) {
    return primary_packageJson;
  }

  // Specifying build is optional
  if(isOption(obj.build)) {
    return defaultSettings;
  }

  const build = refineObject(path.concat('build'), obj.build);
  if(isRefinementError(build)) {
    return build;
  }

  // Specifying settings is optional
  if(isOption(build.settings)) {
    return defaultSettings;
  }

  const settings = refineSettings(path.concat('settings'), build.settings);
  if(isRefinementError(settings)) {
    return settings;
  }

  return mergeSettings(settings, {
    packageJson: primary_packageJson,
    modules: []
  });
}

function refineBuildType_output(path: Array<string>, v: mixed): BuildType_output | RefinementError {
  const obj = attempt(refineObject, path, v, ({}: BuildType_output));
  if(isRefinementError(obj)) {
    return obj;
  }
  const filename = attempt(refineString, path.concat('filename'), obj.filename, "[name]");
  
  const output: BuildType_output = {
    filename
  }

  const _path = refineAny(refineString, refineOption)(path.concat('path'), obj.path);
  if(isRefinementError(_path)) {
    return _path;
  }
  if(!isOption(_path)) {
    output.path = _path;
  }

  const publicPath = refineAny(refineString, refineOption)(path.concat('publicPath'), obj.publicPath);
  if(isRefinementError(publicPath)) {
    return publicPath;
  }
  if(!isOption(publicPath)) {
    output.publicPath = publicPath;
  }

  const library = refineAny(refineString, refineOption)(path.concat('library'), obj.library);
  if(isRefinementError(library)) {
    return library;
  }
  if(!isOption(library)) {
    output.library = library;
  }
  
  const libraryTarget = refineAny(refineString, refineOption)(path.concat('libraryTarget'), obj.libraryTarget);
  if(isRefinementError(libraryTarget)) {
    return libraryTarget;
  }
  if(!isOption(libraryTarget)) {
    output.libraryTarget = libraryTarget;
  }

  return output;
}

function refineBuildType(path: Array<string>, v: mixed): BuildType | RefinementError {
  if(!isObject(v)) {
    return new RefinementError(path, `to be an Object`)
  }

  const implicit = option(refineAny(refineBoolean, refineOption)(path.concat("implicit"), v.implicit), false);
  if(isRefinementError(implicit)) {
    return implicit;
  }
  
  const output = refineBuildType_output(path.concat("output"), v.output);
  if(isRefinementError(output)) {
    return output;
  }

  const entry = (() => {
    // If implicit is set, no entry object is necessary
    if(implicit && isOption(v.entry)) {
      return ({}: {[string]: string});
    }

    // entry is {[string]: string}
    const entry = refineObjectOf<string, string>(refineString, refineString)(path.concat('entry'), v.entry);
    if(isRefinementError(entry)) {
      return entry;
    }

    // Ensure there is at least one entry
    if(!implicit && Object.keys(entry).length === 0) {
      return new RefinementError(path.concat('entry'), 'to specify at least one entrypoint')
    }

    return entry;
  })();
  if(isRefinementError(entry)) {
    return entry;
  }

  if(implicit) {
    return {
      implicit,
      output,
      entry
    }
  } else {
    return {
      output,
      entry
    };
  }
}

const defaultBuild: Build = {
  "web": {
    "implicit": true,
    "entry": {}
  }
}

export function refineBuild(path: Array<string>, v: mixed): Build | RefinementError {
  const obj = refineObject(path, v);
  if(isRefinementError(obj)) {
    return obj;
  }
  const {context, settings, ...possibleBuildTypes} = obj; 

  const buildKeys: Array<'node' | 'web'> = [('node': 'node'), ('web': 'web')];
  return refineObjectOf<'node' | 'web', BuildType>(refineFromEnumArray(...buildKeys), refineBuildType)(path, possibleBuildTypes);
}

export function refinePackageJsonToBuild(path: Array<string>, v: mixed): Build | RefinementError {
  const obj = refineObject(path, v);
  if(isRefinementError(obj)) {
    return obj;
  }

  if(isOption(obj.build)) {
    return defaultBuild;
  }
  
  return refineBuild(path.concat('build'), obj.build);
}

const defaultContext: Context = {
  "entry": './src',
  "output": './dist'
}

export function refinePackageJsonToContext(path: Array<string>, v: mixed): Context | RefinementError {
  const obj = refineObject(path, v);
  if(isRefinementError(obj)) {
    return obj;
  }

  if(isOption(obj.build)) {
    return defaultContext;
  }

  const build = refineObject(path.concat('build'), obj.build);
  if(isRefinementError(build)) {
    return build;
  }

  const contextKeys: Array<'entry' | 'output'> = [('entry': 'entry'), ('output': 'output')];
  const refineContext = refineObjectOf<'entry' | 'output', string>(refineFromEnumArray(...contextKeys), refineString);
  return option(refineAny(refineContext, refineOption)(path.concat('build', 'context'), build.context), defaultContext); 
}

export default function useRefinePackageJson(path: Array<string>, v: mixed): $Diff<BuildConfig, {modulePath: Path}> {
  log(`Refining ${path.join('/')}`)
  const context = useRefinePackageJsonToContext(path, v);
  const settings = useRefinePackageJsonToSettings(path, v);
  const build = useRefinePackageJsonToBuild(path, v);
  return {context, build, settings};
}

const useRefinePackageJsonToBuild = useRefine<Build>(refinePackageJsonToBuild);
const useRefinePackageJsonToContext = useRefine<Context>(refinePackageJsonToContext);
const useRefinePackageJsonToSettings = useRefine<Settings>(refinePackageJsonToSettings);