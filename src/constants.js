// @flow strict

export const Platform = {
    browser: ('browser': 'browser'),
    node: ('node': 'node'),
};
export const Mode = {
  production: ('production': 'production'),
  development: ('development': 'development'),
};
export const Global: typeof Platform & typeof Mode = {
  ...Platform,
  ...Mode
};