// @flow strict
/**
  Takes a list of BuildConfigs and outputs webpack runnable tasks
**/

import makeWebpackConfigs from './config';
import merge from 'webpack-merge';

import makeLog from './log'
const log = makeLog("tasks")

import type {WebpackOptions} from 'webpack';
import { option } from './refinements';

import type { BuildConfig } from './buildSettings/BuildConfig.types';

export default function tasks(configs: Array<BuildConfig>): Array<WebpackOptions> {
  log("Configs:", configs)
  return configs.map(
    makeWebpackConfigs
  ).reduce((acc, v) => acc.concat(v), []);
}
