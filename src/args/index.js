// @flow strict
import makeLog from '../log';
const log = makeLog(); 

import type { Settings } from '../buildSettings/BuildConfig.types';
import useRefineArgs from './refineArgs';

// Set up an argument parser
import { ArgumentParser } from 'argparse';
const parser = new ArgumentParser({
  addHelp: true,
});
parser.addArgument(['-w', '--watch'], {action: 'storeTrue', help: 'Rebuild when the source files change'});
parser.addArgument(['-a', '--analyse'], {action: 'storeTrue', help: 'Output bundle analysis after each build'});
parser.addArgument(['-p', '--production'], {action: 'storeTrue', help: 'Produce production ready bundles'});
parser.addArgument(['--notExecutable'], {action: 'storeTrue', help: 'Prevent node entry point from being executable'});
parser.addArgument(['-m', '--module'], {action: 'storeTrue', help: 'Output a package.json (node)'})
parser.addArgument('modules', {nargs: '+', help: 'Modules to be built'});
function parse(args: Array<string>): Settings {
  const parsed = parser.parseArgs(args);
  return useRefineArgs([], parsed);
}

// Process arguments into settings
export default async function Arguments(args: Array<string>): Promise<Settings> {
  log(`args/index.js: args: ${JSON.stringify(args, null, 2)}`);
  const parsed = parse(args);
  if(!parsed.modules || !parsed.modules.length) {
    throw new Error('No modules specified to build');
  }

  return parsed;
}