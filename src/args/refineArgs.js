// @flow strict
import {
  isRefinementError,
  refineBoolean,
  refineObject,
  refineString,
  refineArrayOf,
  RefinementError,
  useRefine
} from '../refinements';
import type { Settings } from '../buildSettings/BuildConfig.types';

function refineArgs(path: Array<string>, v: mixed): Settings | RefinementError {
  const obj = refineObject(path, v);
  if(isRefinementError(obj)) {
    return obj;
  }

  let settings: Settings = {
    modules: [],
  }

  const modules = refineArrayOf<string>(refineString)(["[modules]"], obj.modules);
  if(isRefinementError(modules)) {
    return modules;
  }
  settings = {
    ...settings,
    modules: modules.concat(modules),
  };

  const watch = refineBoolean(["-w / --watch"], obj.watch);
  if(isRefinementError(watch)) {
    return watch
  }
  if(watch) {
    settings = {
      ...settings,
      watch
    }
  }

  const analyse = refineBoolean(["-a / --analyse"], obj.analyse);
  if(isRefinementError(analyse)) {
    return analyse
  }
  if(analyse) {
    settings = {
      ...settings,
      analyse
    }
  }
  
  const production = refineBoolean(["-p / --production"], obj.production);
  if(isRefinementError(production)) {
    return production
  }
  if(production) {
    settings = {
      ...settings,
      production
    }
  }
  
  const module = refineBoolean(["-m / --module"], obj.module);
  if(isRefinementError(module)) {
    return module
  }
  if(module) {
    settings = {
      ...settings,
      module
    }
  }

  const notExecutable = refineBoolean(["--not-executable"], obj.notExecutable);
  if(isRefinementError(notExecutable)) {
    return notExecutable
  }
  if(notExecutable) {
    settings = {
      ...settings,
      executable: false
    }
  }

  return settings;
}

export default useRefine<Settings>(refineArgs);