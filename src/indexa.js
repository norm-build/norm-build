// @flow strict

import args from './args';
import build from './build';
import buildSettings from './buildSettings'
import type { Settings, BuildConfig } from './buildSettings/BuildConfig.types';
import { isArray, isBoolean, option } from './refinements';
import makeLog from './log';
const log = makeLog("indexa");

import mergeSettings from './mergeSettings';


export default async function(statusFunction: (string | Error) => void, apiSettings: $Rest<Settings, {}>, ...input: Array<string>) {
  log(`index.js: (statusFunction: ${Boolean(statusFunction).toString()}, API Settings: ${JSON.stringify(apiSettings)}, args: ${JSON.stringify(input)})`);

  // Settings preference:
  // API > CLI > packageJson.build > packageJson
  const argSettings = await args(input);
  const settings = mergeSettings(apiSettings, argSettings);
  const _buildSettings: Array<BuildConfig> =( await buildSettings(option(settings.modules, []))).map(s => {

    return {
      ...s,
      settings: mergeSettings(settings, s.settings)
    }
  });
  log(`indexa.js: compiledOptions: ${JSON.stringify(_buildSettings, null, 2)}`);
  return await build(statusFunction)(_buildSettings);
}