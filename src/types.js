// @flow strict
import typeof { Platform, Mode } from './constants';

// $ExpectError: The intention is to use the General Object type here
export type Enum<O: Object = *> = O & {[$Keys<O>]: $ElementType<O, $Keys<O>>};

export type Platforms = {[$Keys<Platform>]: boolean};
export type Modes = {[$Keys<Mode>]: boolean};
export type Globals = {[string]: boolean};
export type ConfigOptions = {mode: $Keys<Modes>, platform: $Keys<Platforms>};