// @flow strict
import type { Settings } from '../buildSettings/BuildConfig.types';
import { isOption, option } from '../refinements';
import makeLog from '../log';
const log = makeLog("settings");
const s = (v) => JSON.stringify(v, null, 2);

type PackageJson = $PropertyType<Settings, "packageJson">;

export default function mergeSettings(...inputSettings: Array<?Settings>): Settings {
  log(`InputSettings: ${s(inputSettings)}`);
  const outputSettings = inputSettings.reduce((acc, v) => {
    if(isOption(v)) {
      return acc;
    }

    if(v.hasOwnProperty("modules")) {
      log(`Modules`);
      acc.modules = option(acc.modules, [])
                      .concat(option(v.modules, []))
                      .filter((item, index, arr) => arr.indexOf(item) === index)
                      ;
    }
    if(v.hasOwnProperty("analyse") && !acc.hasOwnProperty("analyse")) {
      log(`Analyse`);
      acc.analyse = v.analyse;
    }
    if(v.hasOwnProperty("watch") && !acc.hasOwnProperty("watch")) {
      log(`Watch`);
      acc.watch = v.watch;
    }
    if(v.hasOwnProperty("production") && !acc.hasOwnProperty("production")) {
      log(`Production`);
      acc.production = v.production;
    }
    if(v.hasOwnProperty("executable") && !acc.hasOwnProperty("executable")) {
      log(`Executable`);
      acc.executable = v.executable;
    }
    if(v.hasOwnProperty("module") && !acc.hasOwnProperty("module")) {
      log(`Module`);
      acc.module = v.module;
    }
    if(v.hasOwnProperty("packageJson")) {
      log(`PackageJson`);
      const prior: PackageJson = option<PackageJson>(acc.packageJson, ({}: PackageJson));
      acc.packageJson = {
        // $ExpectError: Flow doesn't like double spreading like this
        ...v.packageJson,
        ...prior,
      }
    }


    return acc;
  }, ({}: Settings));
  log(`Resultant settings: ${s(outputSettings)}`);
  return outputSettings;
}