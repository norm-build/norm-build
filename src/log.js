// @flow strict
import { isOption } from './refinements';

function parseFlags(input) {
  return input.split(" ").filter(i => i);
}

const enabled = Boolean(process.env.NORM_BUILD_DEBUG_ENABLED);
const envInputFlags = process.env.NORM_BUILD_DEBUG || "";
const setFlags = new Set(parseFlags(envInputFlags));

function blank(...content: Array<mixed>) {}
function log(...content: Array<mixed>) {
  return console.log(...content);
}

export default(flags?: string) => {
  if(!enabled) {
    return blank;
  }

  // when no flags are set on the log, only output when no flags are set on the environment
  // otherwise log any partially matching flags
  const flagsMatch = parseFlags(flags || "").every((v) => setFlags.has(v))
  if((flags === "" || isOption(flags)) && setFlags.size !== 0) {
    return blank;
  } else if(flagsMatch) {
    return log;
  } else {
    return blank;
  }
}