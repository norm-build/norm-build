// @flow strict
import { absurd } from './utilities';
import merge from 'webpack-merge'
import type { Enum } from './types';
import { option, isOption } from './refinements';
import webpack from 'webpack';
import type { WebpackOptions } from 'webpack';
import babelConfig from './babel.config/index.static';
import path from 'path';
import type { Globals, ConfigOptions } from './types';
import { Global } from './constants';
import NpmDtsPlugin from 'npm-dts-webpack-plugin';
import type { BuildConfig } from './buildSettings/BuildConfig.types';
import makelog from './log';
const log = makelog("config")

// Exclude node_modules from node builds, but include the modules in an emitted package.json
import nodeExternals from 'webpack-node-externals';
import generatePackageJsonPlugin from 'generate-package-json-webpack-plugin';

function makeGlobals(config: BuildConfig): Globals {
  const mode = config.settings.production === true ? "production" : "development";
  const platform = config.build.hasOwnProperty("web") ? "browser" : "node";
  
  return Object.keys(Global).reduce((acc, key: string) => {
    acc[`FLAG_${key}`] = (platform === key || mode === key);
    return acc;
  }, ({}: Globals));
}

function createGeneralConfig(config: BuildConfig): WebpackOptions {
  const generalConfig: WebpackOptions = {};

  const plugins = [];
  plugins.push(
    // $ExpectError: flow-typed does not specify DefinePlugin
    new webpack.DefinePlugin(makeGlobals(config)),
  );
  if(!isOption(config.settings.packageJson)) {
    log("Setting PackageJSON specified", config.settings.packageJson)
    plugins.push(new generatePackageJsonPlugin(config.settings.packageJson, path.join(config.modulePath, "package.json")));
  } else if(option(config.settings.module, false)) {
    plugins.push(new generatePackageJsonPlugin({}, path.join(config.modulePath, "package.json")));
  }
  generalConfig.plugins = plugins;
 
  const externals = []
  if(config.settings.module === true) {
    externals.push(nodeExternals())
  }
  generalConfig.externals = externals

  const output = {};
  if(config.settings.module === true) {
    output.libraryTarget = "commonjs2"
  }

  output.publicPath = "/"
  
  generalConfig.output = output

  if(option(config.settings.production)) {
    generalConfig.mode = 'production';
  } else {
    generalConfig.mode = 'development';
    generalConfig.devtool = ('source-map': 'source-map');
  }

  if(option(config.settings.watch)) {
    generalConfig.watch = config.settings.watch;
  }
  return generalConfig;
} 


function web(config: BuildConfig): WebpackOptions {
  const absoluteContext = {
    entry: path.resolve(path.join(process.cwd()), config.context.entry),
    output: path.resolve(path.join(process.cwd()), config.context.output),
  };

  return {
    resolve: {
      extensions: [
        ".static.ts",
        ".static.tsx",
        ".static.js",
        ".static.json",
        ".static.json5",
        ".static.jsx",
        ".static.css",
        ".static.less",
        ".static.sass",
        ".ts",
        ".tsx",
        ".js",
        ".json",
        ".json5",
        ".jsx",
        ".css",
        ".less",
        ".sass",
      ]
    },
    module: {
        rules: [
            { test: /.*$/,
              oneOf: [
                  // Both static and not static
                  {
                      test: /\.(png|html|jpg|eot|woff|ttf|svg|md)$/,
                      use: {
                        loader: 'file-loader',
                        options: {
                          esModule: false
                        }
                      }
                  },
                  {
                      test: /\.json5$/,
                      use: {
                        loader: 'json5-loader'
                      }
                  },
                  config.settings.module === true ? {
                      test: /\.tsx?$/,
                      exclude: /node_modules/,
                      use: [
                        {
                            loader: 'babel-loader?cacheDirectory',
                            options: babelConfig.web.typescript
                        },
                        "@normed/loaders/checkpoint-loader.js", // resets the mangling ts-loader does to the files
                        {
                          loader: 'ts-loader',
                          options: {
                            context: absoluteContext.entry,
                            configFile: path.join(absoluteContext.entry, "tsconfig.json")
                          }
                        },
                        "@normed/loaders/checkpoint-loader.js",
                        { 
                          loader: "@normed/loaders/resolve-loader.js",  // ts-loader doesn't use webpack resolvers. This loader ensures custom extensions are picked up
                          options: {
                            node: false,
                          }
                        }
                      ],
                  } : {
                      test: /\.tsx?$/,
                      exclude: /node_modules/,
                      use: [
                        {
                            loader: 'babel-loader?cacheDirectory',
                            options: babelConfig.web.typescript
                        },
                      ],
                  },
                  {
                      test: /\.jsx?$/,
                      exclude: /node_modules/,
                      use: [
                          {
                              loader: 'babel-loader?cacheDirectory',
                              options: babelConfig.web.flow
                          }
                      ],
                  },
                  // Static
                  {
                      test: /\.static\.[^\/]*$/,
                      oneOf: [
                        {
                            test: /\.css$/,
                            use: [
                                'raw-loader',
                                'extract-loader',
                                'css-loader',
                            ],
                        },
                        {
                            test: /\.less$/,
                            use: [
                                'raw-loader',
                                'extract-loader',
                                'css-loader',
                                'less-loader',
                            ],
                        },
                        {
                            test: /\.pug/,
                            use: [
                                'raw-loader',
                                'extract-loader',
                                {
                                  loader: 'html-loader',
                                  options: { 
                                    attrs: [
                                      'img:src',
                                      //'script:src',
                                      //'link:href'
                                    ]
                                  }
                                },
                                'pug-html-loader',
                            ]
                        }
                      ]
                  },
                  // Not static
                  {
                      test: /(?!\.static\.)[^\/]*$/,
                      oneOf: [
                        {
                            test: /\.css$/,
                            use: [
                                'style-loader',
                                'css-loader',
                            ],
                        },
                        {
                            test: /\.less$/,
                            use: [
                                'style-loader',
                                'css-loader',
                                'less-loader',
                            ],
                        },
                        {
                            test: /\.pug$/,
                            use: [
                              'pug-loader',
                            ],
                        }
                      ]
                  },
              ]
            },
        ],
    },
    target: 'web',
  };
}
function node(config: BuildConfig): WebpackOptions {
 
  const plugins = [];
  if(option(config.settings.executable, false)) {
    // $ExpectError: flow-typed does not specify BannerPlugin
    plugins.push(new webpack.BannerPlugin({
      banner: '#!/usr/bin/env node',
      raw: true,
      entryOnly: true,
    }));
  }

  const absoluteContext = {
    entry: path.resolve(path.join(process.cwd()), config.context.entry),
    output: path.resolve(path.join(process.cwd()), config.context.output),
  };

  return {
    resolve: {
      extensions: [
        ".static.ts",
        ".static.tsx",
        ".static.js",
        ".static.json",
        ".static.json5",
        ".static.jsx",
        ".ts",
        ".tsx",
        ".js",
        ".json",
        ".json5",
        ".jsx",
      ]
    },
    plugins,
    module: {
        rules: [
            // Only a module requires definitions to be output
            config.settings.module === true ? {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                  {
                    loader: 'babel-loader?cacheDirectory',
                    options: babelConfig.node.typescript,
                  },
                  "@normed/loaders/checkpoint-loader.js", // resets the mangling ts-loader does to the files
                  {
                    loader: 'ts-loader',
                    options: {
                      context: absoluteContext.entry,
                      configFile: path.join(absoluteContext.entry, "tsconfig.json")
                    }
                  },
                  "@normed/loaders/checkpoint-loader.js",
                  {
                    loader: "@normed/loaders/resolve-loader.js",  // ts-loader doesn't use webpack resolvers. This loader ensures custom extensions are picked up
                    options: {
                      node: true
                    }
                  }
                ]
            } : {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                  {
                    loader: 'babel-loader?cacheDirectory',
                    options: babelConfig.node.typescript,
                  }
                ]
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader?cacheDirectory',
                options: babelConfig.node.flow,
            },
            {
                test: /\.(png|html|jpg|eot|woff|ttf|svg|sh|md)$/,
                use: {
                  loader: 'file-loader',
                  options: {
                    esModule: false
                  }
                }
            },
            {
                test: /\.json5$/,
                use: {
                  loader: 'json5-loader'
                }
            },
        ],
    },
    target: 'node',
    node: {
      __dirname: false
    }
  };
}

export default function makeWebpackConfigs(config: BuildConfig): Array<WebpackOptions> {
  const generalConfig = createGeneralConfig(config);

  const webpackOptions: Array<WebpackOptions> = [];

  let platformConfig: WebpackOptions;
  let build: WebpackOptions;

  log("Settings:", config.settings);

  if(config.build.hasOwnProperty("web")) {
    // $ExpectError: Partial build
    const build: WebpackOptions = config.build.web;
    webpackOptions.push(
      merge<WebpackOptions>(build, generalConfig, web(config))
    )
  }

  if(config.build.hasOwnProperty("node")) {
    // $ExpectError: Partial build
    const build: WebpackOptions = config.build.node;
    webpackOptions.push(
      merge<WebpackOptions>(build, generalConfig, node(config))
    )
  }

  return webpackOptions;
}
