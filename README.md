# norm-build

A "simple" build system for js - web and node.

A wrapper round webpack that moves configuration to a declaritive format based on a simple entry in package.json and filenames.

## node usage example

For example, build a node package by setting this in your package.json:

```
// package.json
"build": {
  "settings": {
    "module": true
  },
  "node": {
    "implicit": true
  }
}
```

And adding the "pre-suffix" `.static` to your entrypoints.

```
 |- package.json
 |- src
 |  |- index.static.ts
 |  |- first_file.static.js
 |  \- second_file.js
```

When you run `yarn build`, you'll get

```
 |- package.json
 |- src
 |  |- index.static.ts
 |  |- sheep.jpg
 |  |- first_file.static.js
 |  |- flock.static.jpg
 |  \- second_file.js
 \- dist
    |- index.js
    |- 2df1c6e9149b04608aa35287d2820da9.jpg
    |- flock.jpg
    |- first_file.js
    \- package.json
```

Which is a publishable module!

## web usage example

Control browser compatibility using "browserslist" in package.json

```
  "browserslist": [ "last 2 years" ],
```

or

```
  "browserslist": [ "last 2 years and not dead and > 0.15% and not kaios 2.5" ],
```

see https://browserl.ist/

TODO example web project

# Pre-suffixes

* `static` - causes the file to exist in the output with its pre-suffixless as presented. It will not be renamed or hashed.
* `render` - causes the file to be rendered once before output. rendering occurs after other builds but render order is non-deterministic relative to other renders.

# Development guide

## Debug logging

set the environment variable `NORM_BUILD_DEBUG_ENABLED` to true, and set `NORM_BUILD_DEBUG` to a space seperated list of desired logging flags. Logging flags are specified when `makeLog` is called to produce the `log` functions. There is not currently a central list.